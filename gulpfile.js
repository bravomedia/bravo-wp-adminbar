
// Config
var pluginPath = '.';

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compact'
};

var minifycssOptions = {
  keepBreaks: true
};

var uglifyOptions = {
  output: {
    beautify: true
  }
};

var babelOptions = {
  presets: ['es2015'],
}

var autoprefixerOptions = {
  browsers: ['> 1%', 'IE 8', 'IE 9', 'Firefox > 10', 'iOS > 6'],
  cascade: false,
};

// Autoprefixer fix
require('es6-promise').polyfill();

// Init
var gulp = require('gulp'); 
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var run = require('gulp-run');
//var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var extend = require('extend');
var concat = require('gulp-concat');
var fs = require('fs');

var config = {};
var defaults = {};

if(fs.existsSync('.gulp.json')) {
  config = require('.gulp.json');
}

config = extend(true, defaults, config);

var errorHandler = function(error) {
  console.log(error.toString());
  this.emit('end');
}

var buildSass = function(path) {
  return gulp.src(path +'/sass/*.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass(sassOptions)).on('error', sass.logError)
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(minifycss(minifycssOptions))
    //.pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path +'/build'));
}

var buildES6 = function(path) {
  return gulp.src(path + '/js/*.js')
    //.pipe(sourcemaps.init())
    .pipe(babel(babelOptions)).on('error', errorHandler)
    .pipe(uglify(uglifyOptions))
    //.pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(path + '/build'));
}

gulp.task('build-sass', function() {
  return buildSass(pluginPath + '/public');
});

gulp.task('build-es6', function () {
  return buildES6(pluginPath + '/public');
});

gulp.task('build-admin-sass', function() {
  return buildSass(pluginPath + '/admin');
});

gulp.task('build-admin-es6', function () {
  return buildES6(pluginPath + '/admin');
});

gulp.task('build', function(callback) {
  return runSequence(
    'build-sass',
    ['build-es6', 'build-admin-sass', 'build-admin-es6'],
    callback
  );
});

gulp.task('watch', [], function() {
  gulp.watch(pluginPath + '/public/sass/**/*.scss', ['build-sass']);
  gulp.watch(pluginPath + '/public/js/**/*.js', ['build-es6']);
  gulp.watch(pluginPath + '/admin/sass/**/*.scss', ['build-admin-sass']);
  gulp.watch(pluginPath + '/admin/js/**/*.js', ['build-admin-es6']);
});

gulp.task('default', ['build-sass', 'build-es6', 'build-admin-sass', 'build-admin-es6', 'watch']);
