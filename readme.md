# BravoWP Plugin Template

A simple WP-plugin template.

## Usage
Replace directory and bravo-wp-plugin.php with the name of your plugin.
Delete the admin or public folder if your plugin doesn't provide admin/public functionality.

Run "npm install" and then "gulp watch" to build JS/SASS source.

Add hooks to admin/admin.php and public/public.php.

## TODO
NPM-cleanup, gulp sourcemaps.
