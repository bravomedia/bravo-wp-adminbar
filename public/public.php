<?php

if(!defined('WPINC')) {
    die;
}

add_action('wp_enqueue_scripts', function() use($plugin) {
    if(is_admin_bar_showing()) {
        wp_enqueue_style($plugin->name, $plugin->file_url('public/build/public.css', true), array(), false, 'all');
        //wp_enqueue_script($plugin->name, $plugin->file_url('public/build/public.js', true), array('jquery'), false, false);
    }
});

add_filter('body_class', function($classes) {
    if(is_admin_bar_showing()) {
        $classes[] = 'bravo-wp-adminbar-bottom';
    }
    return $classes;
});
